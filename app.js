// app.js
App({
  onLaunch() {
    // 展示本地存储能力
    console.log("力江小程序启动成功！")
  },
  onShow(){
    console.log("力江小程序从后台进入前台！")
  },
  onHide(){
    console.log('力江小程序成功进入后台工作天了哦！')
  },
  globalData: {
    // 定义全局变量，这个变量可以在别的页面动态的用到
    username: "力江",
    datas: '1',
  }
})
