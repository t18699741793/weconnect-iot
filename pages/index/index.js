// index.js
// 获取应用实例
const app = getApp()

wx.request({
  url: 'http://127.0.0.1:8000/WeChat/blink/',
  method: 'GET',
  success(res) {
    console.log(res.data)
  },
  fail(err) {
    console.error(err)
  }
})

wx.request({
  url: 'http://127.0.0.1:8000/WeChat/blink/',
  method: 'POST',
  data: {
    name: '张三',
    age: 18
  },
  success(res) {
    console.log('请求成功', res.data)
  },
  fail(err) {
    console.error('请求失败', err)
  }
})

Page({
  data: {
    // 在页面中显示的变量，开发中可以页面中直接调用这个变量，但onloda方法里写一个语句
    test1: ''  // 初始化变量
  },
  onLoad() {
    // 页面创建时执行,页面刷新时被调用
    console.log("username:", app.globalData.username)
    app.globalData.username = "艾力"
    var name = "力江程序员"
    this.setData({
      test1:name
    })
  },
})
