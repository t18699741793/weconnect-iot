const app = getApp()
Page({
  data: {
    message: ['努尔艾力', '艾力', '力江'],
    test: '45'
  },
  onLoad(options) {
    const that = this; // 将this赋值给that变量
    wx.request({
      url: 'http://127.0.0.1:8000/WeChat/temperature/',
      method: 'GET',
      success: (res) => {
        console.log(res.data.data);
        this.setData({
          message: res.data.data
        });
      },
      fail: (err) => {
        console.error(err);
      }
    });
  },
})
